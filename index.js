
const cwd = process.cwd();
const fs = require('fs');
const mustache = require('mustache');
const spawnSync = require('child_process').spawnSync;
const puppeteer = require('puppeteer');

const calgen = require('./gen/calendar-gen.js');
const htmlgen = require('./gen/html-gen.js');
const calrend = require('./rend/calendar-rend.js');
const arger = require('./helpers/arguments.js');

const arguments = arger.ProcessArgs(process.argv);

const MongoClient = require('mongodb').MongoClient;

const dbUrl = 'mongodb://localhost:27017';
const databaseName = 'calgendb';
const calendars = 'calendars';

const assert = require('assert');

if (arguments.error) {
    console.log(arguments.error);
    return;
}

const paperSize = 'A3';

const orderJson = arguments.orderJson;
const order = JSON.parse(fs.readFileSync(orderJson, "utf8"));
const mustacheExtension = '.mustache';
const orderNumber = order.id;
const generationDirectory = 'generated/';
let year = order.year;
const files = [];
const zip = order.zip;
const promises = [];
let calendar = {};


let currentMonth = order.month;

if (currentMonth > 12) {
    currentMonth = 1;
    year++;
}

const options = {
    startMonth: currentMonth,
    startYear: year,
    zip: zip,
    lang: 'en',
    havdalah: order.havdalah,
    template: order.style,
    includeParsha: order.includeParsha,
    monthCount: order.monthCount
};



(async () => { 

    await calgen.SetMigrations(options.startYear);

    const res = await calgen.Generate(options, currentMonth);

    const browser = await puppeteer.launch();

    for (const days of res) {
        const month = days.monthidx;
        const filePrefix = `${days.monthidx}-${days.year}`;
        const baseName = filePrefix + '-' + orderNumber + '-' + 'cal';
        const htmlFileName = baseName + '.html';
        const pdfFileName = baseName + '.pdf';
        const htmlFileLocation = generationDirectory + htmlFileName;
        const fileLocation = `${cwd}/generated/${pdfFileName}`;
        const htmlFileLocationURL = `file://${cwd}/${htmlFileLocation}`;

        const html = htmlgen.Generate(options);
        const gennedHtml = mustache.render(html, days);
        
        const headerFileName = currentMonth.toString().padStart(2, "0") + '.html';
        const headerFileLocation = `file://${cwd}/templates/header/${order.image.series}/${headerFileName}`; // 'file:///Users/svavs/Development/Incubator/or-yom/calgen/templates/header/' + order.image.series + '/' + headerFileName;
        const headerFileLocationPdf = `${cwd}/generated/img${pdfFileName}`;
        fs.writeFileSync(htmlFileLocation, gennedHtml);

        
        // @TODO(sjv): Below needs to be fixed - we should move this into another function / process.
        //             We might be able to pre-gen these as well depending on the style to save power
        if (order.image.included) {
                const page = await browser.newPage();
                await page.goto(headerFileLocation, { waitUntil: 'networkidle2' });
                await page.emulateMedia('print');
                await page.pdf({
                    landscape: true,
                    path: headerFileLocationPdf,
                    format: paperSize
                });
            files.push('img' + pdfFileName);
        }

            const page = await browser.newPage();
            await page.goto(htmlFileLocationURL);
            await page.emulateMedia('print');

            await page.pdf({
                landscape: true,
                path: fileLocation,
                format: paperSize

            });

        files.push(pdfFileName);
    }

    await browser.close();
    const { templateId, ...calOptions } = options;

    if(arguments.noCache == false) {
        let optionId = await getDocumentId(calOptions, 'options') || await insertOption(calOptions, 'options'); 
        let calId = await getDocumentId({ optionId: optionId }, 'calendars') || await insertCalendar(res, optionId,'calendars');
        await insertOrder(calId, 'orders', options);
    }


    if (arguments.outputFile) {
        fs.writeFileSync(arguments.outputFile, JSON.stringify(res, null, 2));
    }

    console.log("Beginning stitch...");
    const baseDir = `${cwd}/generated`;
    const pdfUniteArgs = [...files, orderNumber + '.pdf'];
    const pdfUniteOptions = { encoding: 'utf8', cwd: baseDir };
    const pdfunite = spawnSync('pdfunite', pdfUniteArgs, pdfUniteOptions);

    console.log("Cleaning up order directory...");

    files.forEach(file => {
        const fl = baseDir + '/' + file;
        const htmlFl = baseDir + '/' + file.replace('pdf', 'html');

        const removeIfExists = (fileLocation) => {
            if (fs.existsSync(fileLocation))
                fs.unlinkSync(fileLocation);
        };

        removeIfExists(fl);

        if (arguments.savehtml == false)
            removeIfExists(htmlFl);
    });
})();



const getDocumentId = async (select, collectionName) => {
    const client = await MongoClient.connect(dbUrl, { useNewUrlParser: true });

    const db = await client.db(databaseName);
    const collection = await db.collection(collectionName);
    const count = await collection.find(select, {"limit": 1, "projection": {"_id" : 1}}).toArray();
    await client.close();

    if(count.length > 0) return count[0]._id;

    return null;
};


const insertOrder = async (calId, collectionName, opt) => {
    const client = await MongoClient.connect(dbUrl, { useNewUrlParser: true });
    const db = await client.db(databaseName);
    const collection = await db.collection(collectionName);
    
    const order = {
        calendarId: calId,
        orderId: orderNumber,
        options: opt
    };

    await collection.insertOne(order);

    await client.close();
};

const insertOption = async (document, collectionName) => {
    const client = await MongoClient.connect(dbUrl, { useNewUrlParser: true });
    const db = await client.db(databaseName);
    const collection = await db.collection(collectionName);
    
    const doc = {
        ...document
    };

    const d = await collection.insertOne(doc);
    client.close();

    return d.insertedId;
};


const insertCalendar = async (document, optionId, collectionName) => {
    const client = await MongoClient.connect(dbUrl, { useNewUrlParser: true });
    const db = await client.db(databaseName);
    const collection = await db.collection(collectionName);
    
    const doc = {
        optionId,
        cal: document
    };

    const d = await collection.insertOne(doc);
    client.close();

    return d.insertedId;
};


