const axios = require('axios');

exports.GetCalendar = async (opt, month) => {
    const startYear = opt.startYear;
    let numberOfYears = Math.ceil(((opt.startMonth - 1) + opt.monthCount) / 12);
    const results = {};

    try {
        for(let yearOffset = 0; yearOffset < numberOfYears; yearOffset++) {
            const year = startYear + yearOffset;
            const resp = await axios.get(`http://www.hebcal.com/hebcal/?v=1&cfg=json&year=${year}&month=x&maj=on&min=on&nx=on&mf=on&ss=on&mod=on&s=on&c=on&d=on&o=on&geo=zip&zip=${opt.zip}`);
            results[`${year}`] = resp.data;
        }

        results.numberOfYears = numberOfYears;
        return results;
    
    } catch (error) {
        console.error(error);
  }

};