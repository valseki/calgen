exports.Generate = (options, hebrewCalendar) => {
    const calendar = {};
    calendar.hebrewDates = {};
    calendar.holidays = [];
    calendar.parshas = {};
    calendar.candleTimes = {};

    const hebdates = hebrewCalendar.items.filter(hebrewDay => hebrewDay.category === "hebdate");
    for(const day of hebdates) {
        calendar.hebrewDates[day.date] = day.title.substring(day.title.indexOf(','), 0);
    }

    const jewishHolidayResults = hebrewCalendar.items.filter(hebrewDay => ['holiday', 'roshchodesh', 'omer'].includes(hebrewDay.category));
    for(const holiday of jewishHolidayResults) {
        const dateString = holiday.date.split('T')[0];
        const dateParts = dateString.split('-');

        
        const nh = {
            cat: ["Judaism"], 
            emoji: "blank", 
            name: holiday.title, 
            fulldate: dateString, 
            date: { 
                m: dateParts[1], 
                d: dateParts[2]
            } 
        };

        calendar.holidays.push(nh);
    }

    const parshas = hebrewCalendar.items.filter(hebrewDay => hebrewDay.category === 'parashat');
    for(const parsha of parshas) {
        const par = {
            name: parsha.title.substring(parsha.title.indexOf(' ')),
            // @NOTE(sjv): We should move this to a file
            location: parsha.leyning.torah.replace('Genesis', 'Gen.')
                                          .replace('Exodus', 'Ex.')
                                          .replace('Leviticus', 'Lev.')
                                          .replace('Numbers', 'Num. ')
                                          .replace('Deuteronomy', 'Deut.')
        };

        calendar.parshas[parsha.date] = par;
    }

    const candleDays = hebrewCalendar.items.filter(hebrewDay => ['havdalah','candles'].includes(hebrewDay.category));
    for(const candle of candleDays) {
        const dateString = candle.date.split('T')[0];
        calendar.candleTimes[dateString] = candle.title.substring(candle.title.indexOf(':') + 1)
                                            .replace('pm', '').replace('am', '').trim();
    }

    return calendar;
};