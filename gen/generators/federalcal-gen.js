exports.Generate = (date) => {
    return [...getHolidayByDate(date, Fed_Holidays), ...getHolidayUSByDayLocation(date)];
};


const Fed_Holidays = [
    { name: "New Years Day", date: { m: 1, d: 1}, cat : ["federal"], emoji: "celebrate" },
    { name: "Independence Day", date: { m: 7, d:4 }, cat: ["federal"], emoji: "usflag" },
    { name: "Veterans Day", date: { m: 11, d:11 }, cat: ["federal"], emoji: "blank" },
    { name: "Halloween", date: { m: 10, d:31 }, cat: ["secular"], emoji: "pumpkin" },
    { name: "Christmas Day", date: { m: 12, d:25 }, cat: ["christian", "federal"], emoji: "blank" },
];

const getHolidayUSByDayLocation = (date) => {

    const hol = [
            { name: "Martin Luther King, Jr. Day", date: { m: 1, n:1, w: 3 }, cat: ["federal"], emoji: "blank"},
            { name: "George Washington’s Birthday", date: {m: 2, n:1, w:3 }, cat: ["federal"], emoji: "blank" },
            { name: "Memorial Day", date: { m: 5, n:1, w:-1}, cat: ["federal"], emoji: "blank" },
            { name: "Labor Day", date: { m: 9, n:1, w:1}, cat: ["federal"], emoji: "blank" },
            { name: "Columbus Day", date: { m: 10, n:1, w:2}, cat: ["federal"], emoji: "blank" },
            { name: "Thanksgiving", date: { m: 11, n:4, w: 4 }, cat: ["federal"], emoji: "turkey" },
    ];

    const findHolidayFn = (day) => {
        if(day.date.w == -1) {
            if(day.date.m - 1 == date.mon && day.date.n == date.day) {
                return (date.days_in_month - date.num) < 8;
            }
            return false;
        } else {
            return day.date.m - 1 == date.mon && day.date.n == date.day && day.date.w == date.week_num;
        }
    };

    return hol.filter(findHolidayFn);
};

// @CUTNPASTE(sjv)
const getHolidayByDate = (date, arr) => 
{
    const findHolidayFn = (day) => 
    {
        return day.date.m - 1 == date.mon && day.date.d == date.num;
    };

    return arr.filter(findHolidayFn);
};