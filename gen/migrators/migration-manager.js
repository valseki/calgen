const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;


const dbUrl = 'mongodb://localhost:27017';
const databaseName = 'calgendb';

let migrations = [];

exports.SetMigrations = async (year) => {
    const client = await MongoClient.connect(dbUrl, { useNewUrlParser: true });
console.log(year);
    const db = await client.db(databaseName);
    const collection = await db.collection("migrations");
    const records = await collection.find({"year": year, "enabled": true}).toArray();
    await client.close();

    migrations = records;
    console.log("Finished setting the migrations.");
};

exports.Migrate = async (day) => {
    if(day.holidays == undefined) return day;

    const res = migrations.filter((m) => m.date == day.num && m.mon == (day.mon + 1) && m.year == day.year);

    if(res == undefined) return day;

    res.forEach((migration) => { 
        const holiday = day.holidays.find((elem) => elem.name.toLowerCase() == migration.name.original.toLowerCase());
        
        if(holiday) {
            if(migration.name.replacement != undefined)
                holiday.name = migration.name.replacement;

            if(migration.emoji)
                holiday.emoji = migration.emoji;
        }

    });

    return day;
};


