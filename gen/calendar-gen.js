const federalHolidays = require('./generators/federalcal-gen');
const hcg = require('./generators/hebrewcal-gen');
const migrator = require('./migrators/migration-manager');
const hebcalapi = require('./../api/hebcal-api');

let monthNames = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

exports.SetMigrations = async (year) => {
    await migrator.SetMigrations(year);
};

exports.Generate = async (opt, month) => {
    const res = await hebcalapi.GetCalendar(opt, month);
    let startMonth = month - 1;
    let startYear = opt.startYear;
    let lang = opt.lang;
    let resultCal = [];

    for (let monthOffset = 0; monthOffset < opt.monthCount; monthOffset++) {
        let hebrewCalendar = hcg.Generate(opt, res[`${startYear}`]);
        let realMonth = (startMonth + monthOffset) % 12;
        let date = new Date(startYear, realMonth, 1);

        let mon = monthNames[realMonth];
        let listOfDays = [];
        let ci = date.getDay();
        let indexDays = new Array(ci).fill({ isDay: false }, 0, ci);
        let lastDay = addDays(new Date(startYear, realMonth + 1, 1), -1).getDate();;

        while (date.getMonth() == realMonth) {
            let dd = date.getDate();
            let mm = date.getMonth();
            let yyyy = date.getFullYear();

            let dateStr = date.toISOString().slice(0, 10);

            var newDay = {
                mon: mm,
                num: dd,
                day: date.getDay(),
                year: yyyy,
                week_num: Math.ceil(dd / 7), // The week number of occurance
                days_in_month: lastDay,
                h_date: hebrewCalendar.hebrewDates[dateStr],
                c_time: hebrewCalendar.candleTimes[dateStr],
                isDay: true,
                isShabbat: date.getDay() == 5,
                isHavdalah: date.getDay() == 6,
                hasCandle: hebrewCalendar.candleTimes[dateStr] != null && date.getDay() != 5 && date.getDay() != 6,
                parsha: hebrewCalendar.parshas[dateStr]
            };

            let fedHolidays = federalHolidays.Generate(newDay);

            newDay.holidays = [...getHolidayByDate(newDay, hebrewCalendar.holidays), ...fedHolidays];

            let nd = await migrator.Migrate(newDay);

            listOfDays.push(nd);
            indexDays.push(nd);
            date = addDays(date, 1);
        }

        while (ci <= 41) { // (7days * 6weeks) - 1 (0 based index)
            indexDays.push({ isDay: false });
            ci++;
        }

        resultCal.push({ 
            month: mon, 
            year: startYear, 
            monthidx: realMonth + 1, 
            days: listOfDays, 
            index: indexDays 
        });

        if(realMonth >= 11) {
            startYear++;
            await migrator.SetMigrations(startYear);
        }
    }
    
    return resultCal;
};

const getHolidayByDate = (date, arr) => {
    const findHolidayFn = (day) => {
        return day.date.m - 1 == date.mon && day.date.d == date.num;
    };

    return arr.filter(findHolidayFn);
};

const addDays = (dat, days) => {
    const res = new Date(dat.getFullYear(), dat.getMonth(), dat.getDate());
    res.setDate(dat.getDate() + days);
    return res;
};