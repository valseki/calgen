const fs = require('fs');

exports.Generate = (opt) => {
    
    const outlineFilename = opt.template + '-outline-cal.mustache';
    const templateFilename = opt.template + '-cal.mustache';
    const outline = fs.readFileSync('templates/calendar/' + opt.template + '/' + outlineFilename, "utf8");
    const template = fs.readFileSync('templates/calendar/' + opt.template + '/' + templateFilename, "utf8");

    let tableHtml = '';
    let weekOffset = 0;
    let dayOffset = 0;
    
    while(weekOffset < 6) {
        tableHtml += '<tr>\n';
        while(dayOffset < 7) {
            const index = (weekOffset * 7) + dayOffset;
            tableHtml += template.replace(/<<I>>/g, index) + '\n';
            dayOffset++;
        }

        
        tableHtml += '</tr>\n';
        weekOffset++;
        dayOffset = 0;
    }

    const finalHtml = outline.replace(/<< I >>/g, tableHtml);

    return finalHtml;
}