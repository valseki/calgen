

exports.ProcessArgs = (args) => {
    const arguments = { savehtml: false };

    for(let i = 2; i < args.length; i++) {
        const arg = args[i];

        // @REFACTOR(sjv): This can clearly be cleaned up
        if(arg == "--output") {
            const result = getPathForArgument(i, args);
            
            if(result.error) {
                arguments.error = result.error;
                return arguments;
            }

            arguments.outputFile = result;
            continue;
        }

        if(arg == "--order") {

            const result = getPathForArgument(i, args);
            
            if(result.error) {
                arguments.error = result.error;
                return arguments;
            }

            arguments.orderJson = result;
            continue;
        }

        if(arg == "--save-html") {
            arguments.savehtml = true;
            continue;
        }

        if(arg == "--no-cache") {
            arguments.noCache = true;
            continue;
        }
    }

    return arguments;
};

const getPathForArgument = (idx, arr) => {
    const a = { };
    //@BUG(sjv): this is not working correctly...
    if(arr.length <= ++idx) { 
        a.error = "";
        return a;
    }

    const path = arr[idx];
    if(path.length >= 2 && path.substring(0,2) == "--") {
        a.error = "Expected path, receieved argument switch";
        return a;
    }

    return arr[idx];
};
