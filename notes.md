# PDF Gen

wkhtmltopdf -s A3 --zoom 1.2 -O Landscape cal.html cal.pdf

# PDF Merge

pdfunite 01-January.pdf ... 12-December.pdf Calender.pdf
